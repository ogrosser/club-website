/**
 * Copyright (c) 2018 Thomas Rokicki
 */

package application.services.exceptions;

public class DBICreationException extends Exception {

	/**
	 * Auto Generated
	 */
	private static final long serialVersionUID = -5255402911339770961L;

	public DBICreationException() {
		super("Could not initialize the DBI object. Is the DB online?");
	}

	public DBICreationException(String string) {
		super(string);
	}

	public DBICreationException(Exception e) {
		super(e);
	}

	public DBICreationException(String s, Throwable e) {
		super(s, e);
	}

}
