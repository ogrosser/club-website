/**
 * Copyright (c) 2018 Thomas Rokicki
 */

package application.services.helpers;

import java.sql.Date;
import java.util.ArrayList;
import java.util.Arrays;

import org.json.JSONObject;

public class ValidateJsonHelper {
	public static int validateInt(JSONObject json, String id) {
		return json.isNull(id) ? null : json.getInt(id);
	}

	public static boolean validateBoolean(JSONObject json, String id) {
		return json.isNull(id) ? null : json.getBoolean(id);
	}

	public static ArrayList<String> validateStringList(JSONObject json, String id) {
		return json.isNull(id) ? null
				: new ArrayList<String>(
						Arrays.asList(json.getString(id).substring(1, json.getString(id).length() - 1).split(",")));
	}

	public static String validateString(JSONObject json, String id) {
		return json.isNull(id) ? null : json.getString(id);
	}

	public static Date validateDate(JSONObject json, String id) {
		return json.isNull(id) ? null : Date.valueOf(json.getString(id));
	}

}
